
let fruitEmojis = ['🍎', '🍋', '🍇', '🍊', '🍓', '🍍', '🍉','🫐', '🍎', '🍋 ', '🍇', '🍊','🫐' ,'🍓', '🍍', '🍉'];
fruitEmojis.sort(()=>Math.random()-.6);
let timer = undefined;
let seconds= 0;

const eles = document.querySelectorAll(".back");

let i = 0;
for (let tag of eles)
    tag.innerText = fruitEmojis[i++];

const timetag = document.querySelector("span")
const movestag = document.querySelector(".moves");
const tiles = document.querySelectorAll(".tile");

document.querySelector("button").addEventListener("click", ()=>{
    timer = setInterval(()=>{
        timetag.innerText = `${seconds} secs`;
        seconds++;
    },1000);
    const button = document.querySelector("button");
    const nodes = button.childNodes;
    button.disabled = true;
    nodes[0].style.display="none"
    for(let i=1; i<nodes.length; i++)
        nodes[i].style.display = "block";
    tiles.forEach((tile) => {
        tile.classList.add("test")
    })
    setTimeout(()=>tiles.forEach((tile)=>tile.classList.remove("test")), 1000);      
})

let arr = []
let count=0
let moves = 0;
let totalmoves = 0;


tiles.forEach((tile) => {
    tile.addEventListener("click",  (event) => {
        if(arr.length!==0){
            if(!tile.classList.contains("test")){
                if(arr[arr.length-1] !== tile){
                    arr.push(tile);
                }
            }
        }else{
            if(!tile.classList.contains("test")){
                arr.push(tile);
            }
        }
        tile.classList.add("test");
        if(arr.length===2){
            setTimeout(()=>{    
                const curr_tile=arr.shift(), prev_tile=arr.shift();
                if(curr_tile.childNodes[3].innerText !== prev_tile.childNodes[3].innerText){
                    curr_tile.classList.remove("test")
                    prev_tile.classList.remove("test")
                }else{
                    moves+=1;
                    console.log(moves)
                    if (moves==8){
                        clearInterval(timer)
                        document.querySelector(".inform").innerText = "Game finished!"
                    }
                }
            }, 500)
        }
        totalmoves+=1
        movestag.innerText = `Moves ${totalmoves}`;
    })
});